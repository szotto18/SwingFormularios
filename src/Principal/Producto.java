package Principal;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

public class Producto extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtnombre;
    private JTextField txtdescripcion;
    private JTextField txtcantidad;
    private JTextField txtprecio;
    private JTable table1;
    private JButton guardarButton;
    private JButton mostrarButton;
    private JButton eliminarButton;
    private JButton modificarButton;
    private JComboBox comboBox1;
    private JComboBox comboBox2;

    private DefaultTableModel modelproducto;

    public Producto() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        modelproducto=new DefaultTableModel();
        modelproducto.addColumn("Nombre");
        modelproducto.addColumn("Descripcion");
        modelproducto.addColumn("Cantidad");
        modelproducto.addColumn("Precio");
        table1.setModel(modelproducto);

        /*buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });*/

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        guardarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            }
        });
        guardarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String nombre=txtnombre.getText();
                String descripcion=txtdescripcion.getText();
                int cantidad=Integer.parseInt(txtcantidad.getText());
                String precio=txtprecio.getText();

                modelproducto.addRow(new Object[]{
                        nombre, descripcion,precio,cantidad

                } );
                table1.setVisible(false);
                limpiar();


            }
        });
        mostrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                table1.setVisible(true);
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                modelproducto.removeRow(table1.getSelectedRow());
            }
        });
        /**modificarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                modelproducto.removeRow(table1.getEditingRow());
            }
        });*/
        comboBox1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                comboBox1.addItem("001");

            }
        });
    }
    public void limpiar(){
        txtnombre.setText("");
        txtdescripcion.setText("");
        txtprecio.setText("");
        txtcantidad.setText("");
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Producto dialog = new Producto();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
